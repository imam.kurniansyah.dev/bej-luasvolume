public class BinarySearch {
    public static void main (String[] args) {
        int[] arrays = {2, 3, 4, 10, 40, 50, 80, 100};
        int x = 40;
        int result = binarySearch(arrays, 0, arrays.length - 1, x);
        if (result == -1) {
            System.out.println("Element not present");
        } else {
            System.out.println("Element found at index " + result);
        }
    }

    private static int binarySearch(int[] arr, int leftIndex, int rightIndex, int x) {
        if (rightIndex >= leftIndex) {
            System.out.println("Left " + leftIndex);
            System.out.println("Right " + rightIndex);
            int middleIndex = leftIndex + (rightIndex - leftIndex) / 2;
            System.out.println("Middle Value " + arr[middleIndex]);
            System.out.println("------------------------------------");
            System.out.println();
            if (arr[middleIndex] == x) return middleIndex;
            if (arr[middleIndex] > x) return binarySearch(arr, leftIndex, middleIndex - 1, x);
            return binarySearch(arr, middleIndex + 1, rightIndex, x);
        }
        return -1;
    }
}
