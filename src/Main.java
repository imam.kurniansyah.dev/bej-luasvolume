import com.sun.istack.internal.Nullable;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            createHomeMenu();
        } catch (InputMismatchException exception) {
            System.out.println("Input tidak valid");
        }
    }

    private static void createHomeMenu() {
        createMenuHeader("Kalkulator Penghitung Luas dan Volume");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");

        Scanner inputMenu = new Scanner(System.in);
        System.out.print("Pilih menu: ");
        int selectedMenu = inputMenu.nextInt();

        switch (selectedMenu) {
            case 1:
                openHitungLuas();
                break;
            case 2:
                openHitungVolume();
                break;
            default:
                break;
        }
        inputMenu.close();
    }

    private static void openHitungLuas() {
        createMenuHeader("Pilih bidang yang akan dihitung");
        Scanner hitungLuasMenu = new Scanner(System.in);
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");

        System.out.print("Pilih menu: ");
        int selectedMenu = hitungLuasMenu.nextInt();
        switch (selectedMenu) {
            case 1:
                // open input for Persegi
                createMenuHeader("Anda memilih Persegi");
                System.out.print("Masukkan nilai sisi: ");
                int sisi = hitungLuasMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Luas dari Persegi adalah " + calculate(Type.PERSEGI, sisi, sisi, null));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungLuasMenu.nextLine();
                break;
            case 2:
                // open input for Lingkaran
                createMenuHeader("Anda memilih Lingkaran");
                System.out.print("Masukkan nilai jari-jari: ");
                int jariJari = hitungLuasMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Luas dari Lingkaran adalah " + calculate(Type.LINGKARAN, jariJari, null, null));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungLuasMenu.nextLine();
                break;
            case 3:
                // open input for Segitiga
                createMenuHeader("Anda memilih Segitiga");
                System.out.print("Masukkan nilai alas: ");
                int alas = hitungLuasMenu.nextInt();
                System.out.print("Masukkan nilai tinggi: ");
                int tinggi = hitungLuasMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Luas dari Segitiga adalah " + calculate(Type.SEGITIGA, alas, tinggi, null));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungLuasMenu.nextLine();
                break;
            case 4:
                // open input for Persegi Panjang
                createMenuHeader("Anda memilih Persegi Panjang");
                System.out.print("Masukkan nilai panjang: ");
                int panjang = hitungLuasMenu.nextInt();
                System.out.print("Masukkan nilai lebar: ");
                int lebar = hitungLuasMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Luas dari Persegi Panjang adalah " + calculate(Type.PERSEGI_PANJANG, panjang, lebar, null));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungLuasMenu.nextLine();
                break;
            case 0:
                createHomeMenu();
                break;
            default:
                break;
        }
        if (hitungLuasMenu.hasNextLine()) createHomeMenu();
        hitungLuasMenu.close();
    }

    private static void openHitungVolume() {
        createMenuHeader("Pilih bidang yang akan dihitung");
        Scanner hitungVolumeMenu = new Scanner(System.in);
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");

        System.out.print("Pilih menu: ");
        int selectedMenu = hitungVolumeMenu.nextInt();
        switch (selectedMenu) {
            case 1:
                // open input for Kubus
                createMenuHeader("Anda memilih Kubus");
                System.out.print("Masukkan nilai sisi: ");
                int sisi = hitungVolumeMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Volume dari Kubus adalah " + calculate(Type.KUBUS, sisi, sisi, sisi));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungVolumeMenu.nextLine();
                break;
            case 2:
                // open input for Balok
                createMenuHeader("Anda memilih Balok");
                System.out.print("Masukkan nilai panjang: ");
                int panjang = hitungVolumeMenu.nextInt();
                System.out.print("Masukkan nilai lebar: ");
                int lebar = hitungVolumeMenu.nextInt();
                System.out.print("Masukkan nilai tinggi: ");
                int tinggi = hitungVolumeMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Volume dari Balok adalah " + calculate(Type.BALOK, panjang, lebar, tinggi));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungVolumeMenu.nextLine();
                break;
            case 3:
                // open input for Tabung
                createMenuHeader("Anda memilih Tabung");
                System.out.print("Masukkan nilai jari-jari: ");
                int jariJari = hitungVolumeMenu.nextInt();
                System.out.print("Masukkan nilai tinggi: ");
                int tinggiTabung = hitungVolumeMenu.nextInt();
                System.out.print("");
                System.out.println("processing...");
                System.out.print("");
                System.out.println("Volume dari Tabung adalah " + calculate(Type.TABUNG, jariJari, tinggiTabung, null));
                System.out.print("Tekan enter untuk ke menu utama");
                hitungVolumeMenu.nextLine();
                break;
            case 0:
                createHomeMenu();
                break;
            default:
                break;
        }
        if (hitungVolumeMenu.hasNextLine()) createHomeMenu();
        hitungVolumeMenu.close();
    }

    private static String calculate(Type type, int firstValue, @Nullable Integer secondValue,
            @Nullable Integer thirdValue) {
        switch (type) {
            case PERSEGI: // formula: luas persegi = sisi A * sisi B
            case PERSEGI_PANJANG:
                // formula: luas persegi panjang = panjang * lebar
               return String.valueOf(firstValue * secondValue);
            case LINGKARAN:
                // formula: luas lingkaran = 3.14 * jari-jari^2
                return String.valueOf(3.14 * firstValue * firstValue);
            case SEGITIGA:
                // formula: luas segitiga = 0.5 * alas * tinggi
                return String.valueOf(0.5 * firstValue * secondValue);
            case KUBUS:
                // formula: volume kubus = sisi * sisi * sisi
                return String.valueOf(firstValue * secondValue * thirdValue);
            case BALOK:
                // formula: volume balok = 2 * (panjang * lebar + lebar * tinggi + panjang * tinggi)
                int panjang = firstValue;
                int lebar = secondValue;
                int tinggi = thirdValue;
                return String.valueOf(2 * ((panjang * lebar) + (lebar * tinggi) + (panjang * tinggi)));
            case TABUNG:
                // formula: (3.14 * jari-jari * jari-jari) * tinggi
                return String.valueOf((3.14 * firstValue * firstValue) * secondValue);
            default:
                return "Menu yang dipilih tidak ada";
        }
    }

    enum Type {
        PERSEGI,
        LINGKARAN,
        SEGITIGA,
        PERSEGI_PANJANG,
        KUBUS,
        BALOK,
        TABUNG
    }

    // region Utility
    private static void createMenuHeader(String title) {
        int titleLength = title.length();
        createSeparator(titleLength);
        System.out.println(title);
        createSeparator(titleLength);
    }

    private static void createSeparator(int size) {
        String separator = "";
        for (int i = 0; i < size + 3; i++) {
            // ignore the warning. avoiding using any Library except Scanner
            separator += "-";
        }
        System.out.println(separator);
    }
    // endregion
}