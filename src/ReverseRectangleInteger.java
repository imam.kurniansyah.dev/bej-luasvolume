import java.util.Scanner;

public class ReverseRectangleInteger {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Baris segitiga : ");
        int noOfRows = scanner.nextInt();
        int rowCount = noOfRows;

        for (int i = 0; i < noOfRows; i++) {
            // print space di awal dan akhir baris
            for (int j = 1; j <= i * 2; j++) {
                System.out.print(" ");
            }

            /**
             * print angka kecil ke besar
             * case: print 1, 2, 3, 4, 5
             */
            for (int j = 1; j <= rowCount; j++) {
                System.out.print(j + " ");
            }

            /**
             * print besar ke kecil
             * case: print 4, 3, 2, 1
             */
            for (int j = rowCount - 1; j >= 1; j--) {
                System.out.print(j + " ");
            }

            // print kasih enter
            System.out.println();

            // untuk pindah ke baris baru
            rowCount--;
        }
    }
}
