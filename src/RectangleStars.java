import java.util.Scanner;

public class RectangleStars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input lebar persegi : ");
        int width = scanner.nextInt();
        System.out.print("Input panjang persegi : ");
        int length = scanner.nextInt();
        drawRectangle(width, length);
        scanner.close();
    }

    private static void drawRectangle(int width, int length) {
        /**
         * loop pertama:
         * int i = 1
         * jika i kurang dari sama dengan width maka eksekusi block
         */
        for (int i = 1; i <= width; i++) {
            /**
             * loop kedua:
             * int j = 1
             * jika j kurang dari sama dengan length maka eksekusi block
             */
            for (int j = 1; j <= length; j++) {
                /**
                 * jika i == 1: print ***** di atas
                 * jika i == width: print ****** di bawah
                 * jika j == 1: print * di kiri
                 * jika j == length: print * di kanan
                 * else: print space new line
                 */
                if (i == 1 || i == width || j == 1 || j == length) {
                    System.out.print("*");
                } else {
                    // ngasih space ke tengah
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
